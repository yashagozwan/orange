const colors = {
  primary: "#ED1C24",
  secondary: "#F15A24",
  success: "#39B54A",
  dark: "#1A1A1A",
  white: "#ffffff",
};

export default colors;
