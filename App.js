import React from "react";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { Provider as ReducProvider } from "react-redux";
import { Provider as PaperProvider } from "react-native-paper";

import Navigation from "./app/navigation";
import reducers from "./app/reducers";

const App = () => {
  return (
    <ReducProvider store={createStore(reducers, applyMiddleware(thunk))}>
      <PaperProvider>
        <Navigation />
      </PaperProvider>
    </ReducProvider>
  );
};

export default App;
